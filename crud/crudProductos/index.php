<?php

?>
<html>
<head>
	<title>Crud Productos</title>
<!-- En esta seccion es donde se incluyen las hojas de estilos CSS, Librerias JS, tipos de caracteres, etc -->
<!-- Incluimos libreria de JQuery -->
<script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>
<!-- Incluimos nuestras funciones JS -->
<script type="text/javascript" src="js/mainFunciones.js"></script>

</head>

<body>
<!-- Aqui se listan los productos -->
	<div id="contenido_lista">
		<?php include("php/listadoProductos.php"); ?>
	</div>
<!-- Boton para agregar producto -->
	<button type="button" onclick="abre_nuevo_form();">Agregar</button>

</body>

</html>

<style type="text/css">
	th{padding: 10px;background: rgba(225,0,0,.5); color: white;}	
	td{background: rgba(0,0,0,.5); color:white; font-size: 25px;}
	table{position: absolute; width:60%; left: 20%;}
</style>